package dev.inacap.holamundoapr4da12020.modelos;

import android.provider.BaseColumns;

public class BDPincipalContract {
    private BDPincipalContract(){}

    public static final String NOMBRE_BD = "BDPrincipalAPR4DB2020.db";
    public static final int VERSION_BD = 1;

    public static class BDPrincipalUsuarios implements BaseColumns{
        public static final String NOMBRE_TABLA = "usuarios";
        public static final String COLUMNA_NOMBREUSUARIO = "nombreusuario";
        public static final String COLUMNA_PASSWORD = "password";
    }
}
