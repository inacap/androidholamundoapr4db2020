package dev.inacap.holamundoapr4da12020.modelos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDPrincipalHelper extends SQLiteOpenHelper {

    public static final String SQL_TABLA_USUARIOS =
            "CREATE TABLE " + BDPincipalContract.BDPrincipalUsuarios.NOMBRE_TABLA + " (" +
                   BDPincipalContract.BDPrincipalUsuarios._ID + " INTEGER PRIMARY KEY, " +
                    BDPincipalContract.BDPrincipalUsuarios.COLUMNA_NOMBREUSUARIO + " TEXT, " +
                    BDPincipalContract.BDPrincipalUsuarios.COLUMNA_PASSWORD + " TEXT)";

    public BDPrincipalHelper(Context ctx){
        // Contexto, Nombre de la BD, Factory, Version
        super(ctx, BDPincipalContract.NOMBRE_BD, null, BDPincipalContract.VERSION_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // La creacion de las tablas de nuestra BD
        sqLiteDatabase.execSQL(SQL_TABLA_USUARIOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Ejecutar la actualizacion o modificacion de nuestra BD
        // TODO: Programar los pasos para actualizar la BD
    }
}
