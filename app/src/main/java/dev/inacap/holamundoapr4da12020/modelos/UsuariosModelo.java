package dev.inacap.holamundoapr4da12020.modelos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UsuariosModelo {
    private BDPrincipalHelper dbHelper;

    public UsuariosModelo(Context contexto){
        this.dbHelper = new BDPrincipalHelper(contexto);
    }

    public void crearUsuario(ContentValues usuario){
        // Abrir la BD
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();

        // Guardar los datos BD
        db.insert(BDPincipalContract.BDPrincipalUsuarios.NOMBRE_TABLA, null, usuario);
    }

    public ContentValues obtenerUsuario(String nombreUsuario){
        // Consulta SQL
        String consulta = String.format("SELECT * FROM %s WHERE %s = ?",
                BDPincipalContract.BDPrincipalUsuarios.NOMBRE_TABLA, BDPincipalContract.BDPrincipalUsuarios.COLUMNA_NOMBREUSUARIO);

        // Definir parametros
        String[] parametros = new String[]{
                nombreUsuario
        };

        // Ejecutar la consulta y procesar los datos
        SQLiteDatabase bd = this.dbHelper.getReadableDatabase();
        Cursor cursor = bd.rawQuery(consulta, parametros);

        // Procesar los datos
        if(cursor.moveToFirst()){
            // Existe respuesta, y debemos procesar los datos del primer registro

            String cursorNombreUsuario = cursor.getString(cursor.getColumnIndex(BDPincipalContract.BDPrincipalUsuarios.COLUMNA_NOMBREUSUARIO));
            String cursorPassword = cursor.getString(cursor.getColumnIndex(BDPincipalContract.BDPrincipalUsuarios.COLUMNA_PASSWORD));

            // Crear el objeto que debemos retornar
            ContentValues usuario = new ContentValues();

            // agregar los atributos
            usuario.put(BDPincipalContract.BDPrincipalUsuarios.COLUMNA_NOMBREUSUARIO, cursorNombreUsuario);
            usuario.put(BDPincipalContract.BDPrincipalUsuarios.COLUMNA_PASSWORD, cursorPassword);

            return usuario;
        }

        // Indicamos que el usuario no existe
        return null;
    }
}
