package dev.inacap.holamundoapr4da12020;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import dev.inacap.holamundoapr4da12020.controladores.UsuariosControlador;
import dev.inacap.holamundoapr4da12020.vistas.HomeActivity;
import dev.inacap.holamundoapr4da12020.vistas.RegistrarUsuarioActivity;

public class MainActivity extends AppCompatActivity {
    // Declarar componente o widgets
    EditText etNombreUsuario;
    EditText etPass;
    Button btIngresar;
    TextView tvOlvidePass, tvRegistrarme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Verificar si la persona ya habia iniciado sesion
        SharedPreferences sesion = getSharedPreferences("sesion", Context.MODE_PRIVATE);
        boolean yaInicioSesion = sesion.getBoolean("yaInicioSesion", false);

        if(yaInicioSesion){
            Intent abrirSegundaActivity = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(abrirSegundaActivity);
            finish();
        }

        // Instanciar
        etNombreUsuario = findViewById(R.id.etNombreUsuario);
        etPass = findViewById(R.id.etPassword);
        btIngresar = findViewById(R.id.btIngresar);
        tvOlvidePass = findViewById(R.id.tvRecuperarPass);
        tvRegistrarme = findViewById(R.id.tvRegistrarme);

        // Interacccion

        // Detectar el click
        btIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Iniciar una nueva Activity
                String nombreUsuario = etNombreUsuario.getText().toString();
                String password = etPass.getText().toString();

                // Instanciar la capa controlador
                UsuariosControlador capaCont = new UsuariosControlador(getApplicationContext());
                boolean inicioSesion = capaCont.procesarLogin(nombreUsuario, password);

                if(inicioSesion){
                    // Almacenar sesion
                    SharedPreferences sesion = getSharedPreferences("sesion", Context.MODE_PRIVATE);

                    // Objeto para escribir sobre el archivo
                    SharedPreferences.Editor editor = sesion.edit();

                    // agregar la variable que indica que la sesion se inicio
                    editor.putBoolean("yaInicioSesion", true);

                    // guardar los cambios cerrar el archivo
                    editor.commit();

                    Intent abrirSegundaActivity = new Intent(MainActivity.this, HomeActivity.class);
                    startActivity(abrirSegundaActivity);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Datos incorrectos", Toast.LENGTH_LONG).show();
                }

            }
        });

        tvRegistrarme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Iniciar una nueva Activity
                Intent abrirSegundaActivity = new Intent(MainActivity.this, RegistrarUsuarioActivity.class);
                startActivity(abrirSegundaActivity);
            }
        });
    }
}
