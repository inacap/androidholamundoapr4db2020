package dev.inacap.holamundoapr4da12020.vistas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import dev.inacap.holamundoapr4da12020.R;
import dev.inacap.holamundoapr4da12020.controladores.UsuariosControlador;

public class RegistrarUsuarioActivity extends AppCompatActivity {

    private EditText etNombreUsuario, etPass, etPassRepetir;
    private Button btRegistrarme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_usuario);

        this.etNombreUsuario = findViewById(R.id.etNombreUsuario);
        this.etPass = findViewById(R.id.etPassword);
        this.etPassRepetir = findViewById(R.id.etPasswordRepetir);
        this.btRegistrarme = findViewById(R.id.btRegistrarme);

        this.btRegistrarme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombreUsuario = etNombreUsuario.getText().toString();
                String pass = etPass.getText().toString();
                String passRep = etPassRepetir.getText().toString();

                if(!pass.equals(passRep)){
                    etPass.setError("Estos campos deben coincidir");
                    etPassRepetir.setError("Estos campos deben coincidir");
                    return;
                }

                // Comunicar con la capa controlador
                UsuariosControlador cControlador = new UsuariosControlador(getApplicationContext());
                cControlador.crearUsuario(nombreUsuario, pass);

                Toast.makeText(getApplicationContext(), "Usuario registrado!!!", Toast.LENGTH_LONG).show();
            }
        });
    }
}
