package dev.inacap.holamundoapr4da12020.vistas.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import dev.inacap.holamundoapr4da12020.R;

public class BienvenidaFragment extends Fragment{

    private OnFragmentInteractionListener listenerInterno;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Asociar el archivo XML a este framento
        View vistaDelFragment = inflater.inflate(R.layout.fragment_bienvenida, container, false);

        // Interactuar con la interfaz grafica
        Button botonPrueba = vistaDelFragment.findViewById(R.id.btPrueba);

        botonPrueba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getContext(), "Hola desde el Fragment", Toast.LENGTH_SHORT).show();
                listenerInterno.onFragmentInteraction("BienvenidaFragment", "BTPRUEBA_CLICK");

                // Descargar datos desde mindicador.cl
                // servidor: mindicador.cl
                // recurso: /api
                // metodo HTTP: GET

                // Crear la solicitud
                String url = "https://www.mindicador.cl/api";

                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // La solicitud se realizo de manera exitosa
                                Log.i("RESPUESTA", response);

                                // Intepretar la respuesta JSON
                                try {
                                    JSONObject objetoRaiz = new JSONObject(response);
                                    String version = objetoRaiz.getString("version");

                                    JSONObject dolarJSON = objetoRaiz.getJSONObject("dolar");
                                    double valorDolar = dolarJSON.getDouble("valor");

                                    Toast.makeText(getContext(), "Valor del dolar: " + valorDolar, Toast.LENGTH_SHORT).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // Algun error
                            }
                        }
                );

                // Crear lista de solicitudes
                RequestQueue listaEspera = Volley.newRequestQueue(getContext());
                listaEspera.add(solicitud);

                //Toast.makeText(getContext(), "Solicitud enviada", Toast.LENGTH_SHORT).show();

            }
        });

        return vistaDelFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Instanciar el listener para abrir la comunicacion con la activity
        if(context instanceof OnFragmentInteractionListener){
            listenerInterno = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " debe implementar la interfaz OnFragmentInteraction");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listenerInterno = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String nombreFragment, String nombreEvento);
    }
}
