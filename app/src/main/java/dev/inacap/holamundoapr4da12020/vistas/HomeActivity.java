package dev.inacap.holamundoapr4da12020.vistas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import dev.inacap.holamundoapr4da12020.MainActivity;
import dev.inacap.holamundoapr4da12020.R;
import dev.inacap.holamundoapr4da12020.vistas.fragments.BienvenidaFragment;

public class HomeActivity extends AppCompatActivity implements BienvenidaFragment.OnFragmentInteractionListener {

    private Button btLogout, btCambiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        this.btLogout = findViewById(R.id.btLogout);
        this.btCambiar = findViewById(R.id.btCambiar);

        this.btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Almacenar sesion
                SharedPreferences sesion = getSharedPreferences("sesion", Context.MODE_PRIVATE);

                // Objeto para escribir sobre el archivo
                SharedPreferences.Editor editor = sesion.edit();

                // agregar la variable que indica que la sesion se inicio
                editor.putBoolean("yaInicioSesion", false);

                // guardar los cambios cerrar el archivo
                editor.commit();

                Intent abrirSegundaActivity = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(abrirSegundaActivity);
                finish();
            }
        });

        this.btCambiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Mostrar el fragment
                // Debemos instanciarlo
                BienvenidaFragment fragmento = new BienvenidaFragment();

                // FragmentManager para administrar los fragmentos en esta activity
                FragmentManager manager = getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.fgContenedor, fragmento).commit();
            }
        });
    }

    @Override
    public void onFragmentInteraction(String nombreFragment, String nombreEvento) {
        if(nombreFragment.equals("BienvenidaFragment")){
            if(nombreEvento.equals("BTPRUEBA_CLICK")){
                //Toast.makeText(getApplicationContext(), "Hola desde la Activity", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
