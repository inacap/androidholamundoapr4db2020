package dev.inacap.holamundoapr4da12020.controladores;

import android.content.ContentValues;
import android.content.Context;

import dev.inacap.holamundoapr4da12020.modelos.BDPincipalContract;
import dev.inacap.holamundoapr4da12020.modelos.UsuariosModelo;

public class UsuariosControlador {
    private UsuariosModelo capaModelo;

    public UsuariosControlador(Context context){
        this.capaModelo = new UsuariosModelo(context);
    }

    public void crearUsuario(String nombreUsuario, String password){
        // Registrar un nuevo usuario

        // Crear el nuevo usuario en memoria
        ContentValues nuevoUsuario = new ContentValues();

        // definir este objeto con atributos en formato clave valor
        nuevoUsuario.put(BDPincipalContract.BDPrincipalUsuarios.COLUMNA_NOMBREUSUARIO, nombreUsuario);
        nuevoUsuario.put(BDPincipalContract.BDPrincipalUsuarios.COLUMNA_PASSWORD, password);

        this.capaModelo.crearUsuario(nuevoUsuario);

    }

    public boolean procesarLogin(String nombreUsuario, String password){
        ContentValues usuarioBD = this.capaModelo.obtenerUsuario(nombreUsuario);

        // Existe el usuario?
        if(usuarioBD == null){
            // No existe
            return false;
        }

        // Validar password
        String passUsuario = usuarioBD.getAsString(BDPincipalContract.BDPrincipalUsuarios.COLUMNA_PASSWORD);
        if(passUsuario.equals(password)){
            return true;
        }

        return false;
    }
}
